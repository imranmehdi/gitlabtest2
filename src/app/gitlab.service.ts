import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlService } from './url.service';

const ACCESS_TOKEN = 'c23b8c8f76f4e67923b738f59ab09e655b6e99e6e2f5d5dbe95afad4b18b7b63';

@Injectable({
  providedIn: 'root'
})
export class GitlabService {

  constructor(private urlService: UrlService, private httpClient: HttpClient) { }

  getCurrentUser() {
    return this.httpClient.get(this.urlService.getCurrentUserUrl(ACCESS_TOKEN));
  }

  getProjects() {
    return this.httpClient.get(this.urlService.getProjectsUrl(ACCESS_TOKEN));
  }

  getUserProjects(userId: string) {
    return this.httpClient.get(this.urlService.getUserProjectsUrl(userId, ACCESS_TOKEN));
  }

  getBranches(projectId: string) {
    return this.httpClient.get(this.urlService.getBranchesUrl(projectId, ACCESS_TOKEN));
  }

  getEnvironments(projectId: string) {
    return this.httpClient.get(this.urlService.getEnvironmentsUrl(projectId, ACCESS_TOKEN));
  }

  getGroups() {
    return this.httpClient.get(this.urlService.getGroups(ACCESS_TOKEN));
  }
}
