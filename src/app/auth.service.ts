import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  getAccessToken(credentials: any) {
    this.httpClient.post('https://gitlab.com/oauth/token', credentials, httpOptions)
      .subscribe(data => console.log(data));
  }
}
