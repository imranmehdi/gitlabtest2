import { Injectable } from '@angular/core';

export const GITLAB_API_URL = 'https://git.drooms.com/api';
export const GITLAB_API_VERSION = 'v4';

@Injectable({
  providedIn: 'root'
})
export class UrlService {

  constructor() { }

  baseUrl() {
    return `${GITLAB_API_URL}/${GITLAB_API_VERSION}`;
  }

  getCurrentUserUrl(accessToken: string) {
    return this.baseUrl() + `/user?access_token=${accessToken}`;
  }

  getBranchesUrl(projectId: string, accessToken: string) {
    return this.baseUrl() + `/projects/${projectId}/repository/branches?access_token=${accessToken}`;
  }

  getUserProjectsUrl(userId: string, accessToken: string) {
    return this.baseUrl() + `/users/${userId}/projects?access_token=${accessToken}`;
  }

  getEnvironmentsUrl(projectId: string, accessToken: string) {
    return this.baseUrl() + `/projects/${projectId}/environments?access_token=${accessToken}`;
  }

  getGroups(accessToken: string) {
    return this.baseUrl() + `/groups/4?access_token=${accessToken}`;
  }

  getProjectsUrl(accessToken: string) {
    return this.baseUrl() + `/groups/4/projects?access_token=${accessToken}`;
  }
}
