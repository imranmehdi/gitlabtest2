import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-drs-sign-in',
  templateUrl: './sign-in.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SsoSignInComponent implements OnInit {

  email: '';
  password: '';

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  login() {
    const credentials = {
      grant_type : 'password',
      username : 'imranmehdi@mailbox.org', // this.email,
      password : '18051992Im.' // this.password
    };

    console.log(credentials);

    this.authService.getAccessToken(credentials);
  }

}
