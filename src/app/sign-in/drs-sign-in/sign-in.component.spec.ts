import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SsoSignInComponent } from './sign-in.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('SsoSignInComponent', () => {
  let component: SsoSignInComponent;
  let fixture: ComponentFixture<SsoSignInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [ SsoSignInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SsoSignInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
