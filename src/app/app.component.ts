import { Component, OnInit } from '@angular/core';
import { GitlabService } from './gitlab.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  projects$: Observable<any>;
  userProjects$: Observable<any>;
  branches$: Observable<any>;
  environments$: Observable<any>;
  currentUser$: Observable<any>;
  groups$: Observable<any>;

  selectedProjectId: string;

  constructor(private service: GitlabService) {}

  ngOnInit() {
    this.getCurrentUser();
    this.getGroups();
  }

  getCurrentUser() {
    this.currentUser$ = this.service.getCurrentUser();
  }

  getGroups() {
    this.groups$ = this.service.getGroups();
  }

  getProjects() {
    this.projects$ = this.service.getProjects();
  }

  getUserProjects(userId) {
    this.userProjects$ = this.service.getUserProjects(userId);
  }

  getBranches(projectId: string) {
    this.selectedProjectId = projectId;
    this.branches$ = this.service.getBranches(this.selectedProjectId);
  }

  getEnvironments() {
    this.environments$ = this.service.getEnvironments(this.selectedProjectId);
  }
}
